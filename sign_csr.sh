#!/usr/bin/env bash
set -e

prompt_for_ext_section () {
  sections=$(grep -i '\[[[:space:]]*ext_section_.*\]' "$config_file" | \
     sed -E -e 's/\[[[:blank:]]*ext_section_([^[:blank:]]*)[[:blank:]]*\]/\1/g')
  echo "Please select which set of extensions the certificate should have:"
  select section in $sections; do
    if [ -z "$section" ]; then
      echo "$0: no such ext_section, try again" >&2
    else
      break
    fi
  done

  #entering EOF causes select to exit with section not being set
  if [ -z "$section" ]; then
    echo "$0: no extenstions selected, stop" >&2
    exit 4
  fi

  ext_section="ext_section_$section"
}

config_file="ca.cnf"
valid_days="3650"
ca_name="ca"
ca_dir="$(pwd)"

while (("$#")); do
  case "$1" in
    -c|--config)
      config_file="$2"
      ;;
    -d|--days)
      valid_days="$2"
      ;;
    --ca-name)
      ca_name="$2"
      ;;
    -s|--ca-dir)
      ca_dir="${2%/}"
      ;;
    -e|--extensions)
      ext_section="$2"
      ;;
    -n|--csr-file)
      csr_file="$2"
      ;;
    -h|--help)
      echo "
Usage: ./sign_csr.sh [options] -n <file>
Signs the CSR provided by the -n option into a certificate. The certificate will have the same filename, but with a .csr replaced with .crt, and will be saved in the same directory as the CSR.
Options:
  -c --config <file>: the OpenSSL config file to use-
  -d --days <number>: the number of days the new certificate should be valid.
  --ca-name <name>: the name of the CA used to sign the new certificate. The CA crt need to have the filename <name>.crt and the private key needs to be called <name>.key.
  -s --ca-dir <dir>: the directory where the signing CA's certificate and private key can be found.
  -e --extensions <section name>: the name of the sections of the OpenSSL config from which the new certificates extensions should be taken.
  -n --csr-file <file>: mandatory. The CSR file to be signed and turned into a certificate. The CSR filename needs to end with .csr.
  -h --help: show this help and exit.
"
      exit 0
      ;;
    *)
      echo "$0: unrecognized option $1" >&2
      exit 1
  esac
  shift 2
done

if ! [ -f "$config_file" ]; then
  echo "$0: no config file $config_file" >&2
  exit 2
fi

if ! [[ "$valid_days" =~ ^[0-9]+$ ]]; then
  echo "$0: $valid_days is not a number" >&2
  exit 2
fi

ca_key="$ca_dir/$ca_name.key"
ca_crt="$ca_dir/$ca_name.crt"

if [ ! -r "$ca_key" ] || [ ! -r "$ca_crt" ]; then
  echo "$0: cannot read the ca crt/key files, $ca_crt and $ca_key" >&2
  exit 2
fi

if ! [[ "$csr_file" =~ \.csr$ ]]; then
  echo "$0: the CSR file has to have the .csr ending" >&2
  exit 2
elif [ ! -r "$csr_file" ]; then
  echo "$0: cannot read the csr file $csr_file" >&2
  exit 2
fi
crt_file="${csr_file/%csr/crt}"
if [ -f "$crt_file" ]; then
  echo "$0: the certificate file $crt_file already exists!" >&2
  exit 2
fi

if [ -z "$ext_section" ]; then
  prompt_for_ext_section
fi

database="$(dirname $config_file)/db"
database_serials="$database/cert_serials_to_names.txt"
cert_serial="$(cat $database/next_serial.txt)"

openssl ca -in "$csr_file" -out "$crt_file" -config "$config_file" -days "$valid_days" -cert "$ca_crt" -keyfile "$ca_key" -extensions "$ext_section"

if [ ! -f "$crt_file" ]; then
  echo "$0: crt was not created!" >&2
  exit 3
fi

echo "$cert_serial,$(basename -- $crt_file),$(date)" >> "$database_serials"

echo "Done!"

#!/usr/bin/env bash
set -e

# Tip: you can print the contents of your csr using 'openssl req -text -noout -in <csr-file>'

workdir=$(pwd)
use_passphrase="yes"

print_help() {
  echo "
Usage: ./gen_csr.sh [options] [<workdir>] <name>
Creates a new private key and associated Certificate Signing Request (CSR), called <name>.key and <name>.csr respectively, in the directory <workdir>.
Options:
  -p --no-pass: don't use a passphrase for the private key
  -h --help: print this help message and exit
  <workdir>: the directory to save the key and csr files in.
  <name>; mandatory. The name of the key and csr files.
"
}

done_parse="no"
while (("$#")) && [ "$done_parse" = no ]; do
  case "$1" in
    -p|--no-pass)
      use_passphrase="no"
      shift 1
      ;;
    -h|--help)
      print_help
      exit 0
      ;;
    *)
      done_parse="yes"
      ;;
  esac
done

if [ "$#" -gt 2 ] || [ "$#" -lt 1 ]; then
  print_help
  exit 1
fi

if [ "$#" -eq 1 ]; then
  name="$1"
else
  workdir="${1%/}"
  name="$2"
fi

keyfile="$workdir/$name.key"
csrfile="$workdir/$name.csr"
subject="/C=SE/O=rigoletto/CN=$name"
gen_key_opts=""

if [ "$use_passphrase" = yes ]; then
  gen_key_opts="-p"
fi

./int_gen_key.sh $gen_key_opts "$keyfile" && \
  openssl req -out "$csrfile" -new -key "$keyfile" -subj "$subject" || \
  rm "$keyfile"

if [ ! -f "$csrfile" ]; then
  echo "$0: csr was not created!" >&2
  exit 2
fi

echo "Created keyfile $keyfile - this is your private key!"
echo "Created CSR $csrfile - send this to your CA for signing"
echo "You can inspect the contents of your csr by doing 'openssl req -text -noout -in $csrfile'"
echo "Done"

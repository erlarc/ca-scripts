#!/usr/bin/env bash
set -e

valid_days=3650
workdir="$(pwd)"
name="ca"
use_passphrase="yes"

config_file="ca.cnf"

while (("$#")); do
  case "$1" in
    -d|--days)
      valid_days="$2"
      shift 2
      ;;
    -w|--workdir)
      workdir="${2%/}"
      shift 2
      ;;
    -n|--name)
      name="$2"
      shift 2
      ;;
    -c|--config)
      config_file="$2"
      shift 2
      ;;
    -p|--no-pass)
      use_passphrase="no"
      shift 1
      ;;
    -h|--help)
      echo "
Usage: ./gen_ca.sh [options]
Generate a self-signed certificate, suitable for use as a root CA.
Options:
  -d --days <number>: number of days the root certificate will be valid.
  -w --workdir <dir>: the directory where the certificate and key will be stored.
  -n --name <name>: the certificate will be named <name>.crt and the private key will be named <name>.key.
  -c --config <file>: the OpenSSL config file to use.
  -p --no-pass: don't use a passphrase for the private key file
  -h --help: print this help and exit.
"
      exit 0
      ;;
    *)
      echo "$0: unrecognized option $1" >&2
      exit 1
      ;;
  esac
done

if [ ! -d "$workdir" ]; then
  echo "$0: $workdir doesn't exist or isn't a directory" >&2
  exit 2
fi
if [ -z "$name" ]; then
  echo "$0: no name specified" >&2
  exit 2
fi
if [ ! -f "$config_file" ]; then
  echo "$0: config file $config_file doesn't exist" >&2
  exit 2
fi
if ! [[ "$valid_days" =~ ^[0-9]+$ ]]; then
  echo "$0: $valid_days is not a valid number" >&2
  exit 2
fi

ca_file="$workdir/$name.crt"
key_file="$workdir/$name.key"

subject_dn="/C=SE/O=rigoletto/CN=$name"
gen_key_opts=""
if [ "$use_passphrase" = yes ]; then
  gen_key_opts="-p"
fi

./int_gen_key.sh $gen_key_opts "$key_file" && \
  openssl req -x509 -out "$ca_file" -new -config "$config_file" -days "$valid_days" -key "$key_file" -subj "$subject_dn" || \
  rm "$key_file"

if [ ! -f "$ca_file" ]; then
  echo "$0: $ca_file was not created!" >&2
  exit 3
fi

echo "CA created as $ca_file, contents:"
echo ""

openssl x509 -noout -in "$ca_file" -text

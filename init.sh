#!/usr/bin/env bash
set -e

template_conf_file="ca.cnf.template"
new_conf_file="ca.cnf"
install_dir="$PWD"

while (($#)); do
  case "$1" in
    -t|--template)
      template_conf_file="$2"
      ;;
    -c|--config)
      new_conf_file="$2"
      ;;
    -d|--install-dir)
      install_dir="${2%/}"
      ;;
    -h|--help)
      echo "
Usage: ./init.sh [options]
Sets up the files and directories needed to use gen_ca.sh and sign_csr.sh. Not needed for gen_csr.sh.
Options:
  -t --template <file>: the OpenSSL config tempalte to use.
  -c --config <name>: the name for the 'real' config file to generate from the template
  -d --install-dir <name>: the directory where everything should be set up.
  -h --help: print this help and exit.
"
      exit 0
      ;;
    *)
      echo "$0: unrecognized option $1" >&2
      exit 1
      ;;
  esac
  shift 2
done

if [ ! -d "$install_dir" ]; then
  if ! mkdir "$install_dir" ; then
    echo "$0: could not create the installation directory $install_dir" >&2
    exit 2
  fi
fi
install_dir=$(realpath "$install_dir")
sed -e "s|@HOME@|$install_dir|" < "$template_conf_file" > "$install_dir/$new_conf_file"

# copy scripts
for script in init.sh gen_ca.sh gen_csr.sh sign_csr.sh int_gen_key.sh ; do
  cp "$script" "$install_dir/"
done

# create database
mkdir "$install_dir/db"
mkdir "$install_dir/db/certs"

echo "# This file records the filename of the CSR that created the certificate and the date that was done" > "$install_dir/db/cert_serials_to_names.txt"
echo "# SERIAL,CRT-FILENAME,DATE" >> "$install_dir/db/cert_serials_to_names.txt"

touch "$install_dir/db/certs.txt"

next_serial=$(openssl rand -hex 16)
echo ${next_serial^^} > "$install_dir/db/next_serial.txt"

dd if=/dev/random of="$install_dir/db/randfile" bs=256 count=1 >/dev/null 2>&1

echo "You should consider chaning the access rights to the files and directories in $install_dir to more restrictive ones."
echo "Done"

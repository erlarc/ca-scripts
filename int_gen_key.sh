#!/usr/bin/env bash
set -e

# Generate a single private key file

done_parse="no"
while (("$#")) && [ "$done_parse" = "no" ] ; do
  case "$1" in
    -p|--passphrase)
      use_passphrase="yes"
      shift 1
      ;;
    *)
      done_parse="yes"
      ;;
  esac
done

if [ "$#" -ne 1 ]; then
  echo "$0: exactly one output file must be specified" >&2
  exit 1
else
  output_file="$1"
fi

if [ -f "$output_file" ]; then
  echo "$0: keyfile $output_file already exists! Make sure this isn't needed, since overwriting a key might make it impossible to recover!" >&2
  exit 2
fi

if [ -n "$use_passphrase" ]; then
  read -p "Enter new passphrase for $output_file (min 5 characters):" -r -s passphrase
  echo ""
  if [ "${#passphrase}" -le 4 ]; then
    echo "$0: passphrase too short!" >&2
    exit 4
  fi

  read -p "Repeat passphrase:" -r -s passphrase_verify
  echo ""

  if ! [ "$passphrase" = "$passphrase_verify" ]; then
    echo "$0: passphrases don't match!" >&2
    exit 4
  fi
fi

echo "$passphrase" | openssl genpkey -algorithm ed25519 -out "$output_file" ${use_passphrase:+-pass stdin -aes256}

if [ ! -f "$output_file" ]; then
  echo "$0: keyfile could not be generated!" >&2
  exit 3
fi
